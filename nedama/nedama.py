# Nedama Library (Nemobile Data Manager)
# Author: Victor Mendoza
# Created: 21/04/2019
# Modified: 27/04/2019

import pandas as pd

class Nedama():
	"""
		Nedama Object

			This will manage all the current data used with 
			Brainep Object

			Attributes:

				original_data -> Original data to use
				processed_data -> Data processed after clean_data function
				model_data -> Data processed by a Model by Brainep Object
				final_data -> Data response
	"""
	original_data = None
	processed_data = None
	model_data = None
	final_data = []

	def __init__(self,data=None,file_type="csv"):
		"""
			Construct will initialize original_data attribute

			Parameters:
				data -> Data to be processed, in JSON format

		"""	
		self.original_data = pd.read_csv(data) if file_type=="csv" else data

	def clean_data(self,columns):
		"""
			Function to keep only the columns needed for training in processed_data attribute

			Parametrs:
				columns -> List of columns name to keep


		"""
		self.processed_data = self.original_data[columns]

	def show_data(self,data_type="processed"):
		"""
			Function to sse current data

			Parameters:
				data_type -> 
					"processed" -> Processed data after clean_data function,
					"original" -> Original data passed in the construct
					"model" -> Model data
		"""
		return self.original_data if data_type=="original" \
			else self.processed_data if data_type=="processed" \
				else self.model_data if data_type=="model"\
					else ""

	def model_data(self,data):
		"""
			Store data in model_data from some model in Brainep instance

			Parameters:
				data -> Data from model

		"""
		self.model_data = data

	def geocluster_data(self):
		"""
			Function to transform data into json
		"""
		pass