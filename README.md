Sidis

AI used by Sidis for multiple purposes

Contain two packages:

-> Nedama (Nemobile Data Manager) 
	This package will be used to manage the input, processed and how  the output data will be

-> Brainep (Brain's Nemobile Processor)
	This package will be used to create diferent kind of models
	
	Brainep is a class used to have control over:
	
		resources -> All the models currently used by Brainep
	
		current_model -> Currently model in use (By default is the first model)

		model_types -> Tuple of all the currently support models

	Inside this class are classes based on models to train and  predict as base for each class, there can be more functions based on  which type of model are needed

	Currently support Model:

		GeoCluster -> Class for clustering and predictions based  in coordinates, also has a function to obtain the most center point in  each cluster

			
