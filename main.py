# Example
# Author: Victor Mendoza
# Created: 21/04/2019
# Modified: 27/04/2019

from nedama.nedama import Nedama
from brainep.brainep import Brainep

data = Nedama(data="data/dataset.csv")

data.clean_data(['latitude','longitude'])

brain = Brainep(0,data=data.processed_data,kms=1,min_samples=2)

brain.current_model.train_model()

brain.current_model.predict_cluster([38.655245,38.519470], [-121.0759,-121.435768])

print(brain.current_model.predict_data)
	