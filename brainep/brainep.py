
# Brainep Brain's Nemobile Processor
# Author: Victor Mendoza
# Created: 21/04/2019
# Modified: 27/04/2019

import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.neighbors import DistanceMetric

class Brainep():
	"""
		Brainep Object

			This will manage all the models used
			with an instance

			Attributes:

				resources -> A list of dict values where 
					are listed each model

					[
						{
							'model_type' : int,
							Model_name : Model Object
						}
					]

					Model_name and Model  Object are getting
					from model_types tuple

				current_model -> Current model in use, by default is the
					first model in the resources list

				model_types -> Tuple of available models
					(
						(
							model_number,
							model_name,
							class_model,
						)
					)

					model_number is the position of the model, is used to select
					model

					model_name is the name used in the resources attribute to refer
					to the model

					class_model is the class object model used with his model (is 
					not an instance)
		"""

	resources = []
	current_model = None

	def __init__(self,model_type=None,**kwargs):
		"""
			Brainep Construct
			
			Parameters:

				model_type -> Can be an integer
					to creatte one model or a list of integer 
					to create multiple models

				**kwargs -> Arguments for model type
					'data' -> Data to use with the model
		"""
		try:
			# Acttivate one model
			model_type = int(model_type)
			self.resources.append(
				{
					'model_type' : model_type,
					self.models_type[model_type][1] : self.models_type[model_type][2](**kwargs),
				}
			)
		except TypeError:
			# Activate multiple models
			for model in model_type:
				self.resources.append(
					{
						'model_type' : model,
						self.models_type[model][1] : self.models_type[model][2](**kwargs),
					}
				)

		self.change_current_model(0)

	def change_current_model(self,model):
		"""
			Change the current model used by his numer

			Parameters
				
				model -> Number of the model in the resources attribute
		"""
		self.current_model = self.resources[model][self.models_type[self.resources[model]['model_type']][1]]

	class GeoCluster():
		"""
			GeoCluster Object (Model):

				This is a model for clusterring coordinates based
				in haversine metric, also can be predict new points in the cluster
				and obtain the most center point in the cluster 

				Attributes:

				KMS -> Constant used to convert kms to radians
				original_data -> Original data used by the model
				processed_data -> Data processed after the train_model function
				model -> Current mode

		"""

		KMS = 6371.0088
		original_data = None
		processed_data = None
		model = None
		num_clusters = None
		predict_data = []

		def __init__(self,**kwargs):
			"""
				Initialize DBSCAN Model for clustering

				Initialize data
			"""
			kwargs['kms'] = int(kwargs['kms']) if 'kms' in kwargs else 1
			kwargs['min_samples'] = int(kwargs['min_samples']) if 'min_samples' in kwargs else 2
			self.model = DBSCAN(
				eps= kwargs['kms']/self.KMS,
				min_samples= kwargs['min_samples'],
				algorithm='ball_tree',
				metric='haversine'
			)
			self.original_data = kwargs['data']

		def train_model(self):
			"""
				Function to train model based on original_data attribute

				After training the processed_data is changed with the labels 
				of each element

				After traning the num_cluster is change to the size of the cluster
				obtained in the training
			"""
			self.model.fit(np.radians(self.original_data))
			self.processed_data = self.model.labels_
			self.num_clusters = len(set(self.processed_data))

		def predict_cluster(self,lat,lon):
			"""
				Function to predict cluster of one or multiple points

				V1.0:
					
					Parameters:

						lat -> (float) or list (float) of lat 
						lon -> (float) or list (float) of lon

					Attributes:

						samples -> Number of new points to predict his cluster
						prediction -> Cluster prediction of the points
						new_point -> Ndarray of the points passed as arguments
						dist -> DistanceMetric Object to calculate haversine metric

			"""
			# First verify if there's one or multiple points
			try:
				samples = len(lat)
				is_list = True
			except TypeError:
				samples = 1
				is_list = False

			# Create a prediction numpy ndarray an initialize as noise (-1)
			prediction = np.ones(shape=samples,dtype=int)*-1
			# Convert point or points into ndarray
			if is_list:
				# If exist multiple points
				new_point = []
				for i,j in zip(lat,lon):
					new_point.append([i,j])
				new_point = np.ndarray(shape=(samples,2),buffer=np.radians(np.array(new_point)))
			else:
				# if exist only one point
				new_point = np.ndarray(shape=(samples,2),buffer=np.radians(np.array([lat,lon])))
			#Construct of DistanceMetric for haversine distance
			dist = DistanceMetric.get_metric('haversine')
			# Iterate each new_point to obtain a prediction for cluster
			for i,point in enumerate(new_point):
				# Iterate all the cluster compoenets of the model
				for j,core_point in enumerate(self.model.components_):
					# Calculate if the distance of the point and core point
					# are less of eps of the model
					if dist.pairwise(X=np.ndarray(shape=(1,2),buffer=point),
						Y=np.ndarray(shape=(1,2),buffer=core_point)) < self.model.eps:
						# We save the number of the cluster with the predicttion
						prediction[i] = self.model.labels_[self.model.core_sample_indices_[j]]

			# After getting the prediction we return the items in the cluster
			for cluster in prediction:
				self.predict_data.append(np.where(self.processed_data==cluster))

		def get_most_center_points(self):
			pass
			
	models_type = (
		(0, 'GeoCluster',GeoCluster),
	)